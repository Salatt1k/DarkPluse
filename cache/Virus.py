import os
import sys
import subprocess
import time
import random
# Добавляем путь к корневой папке проекта
project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'llib'))
sys.path.append(project_path)
import config
from termcolor import colored

text = 'Warning: с учётом того что программа регулярно обновляется, данная функция может работать не на всех версиях Android...'
color = (colored(text, 'cyan'))

def printer(text, delay=0.035):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(delay)
    print()  # для перехода на новую строку после завершения печати

def open_file(file_path):
    subprocess.run(['python', file_path])

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    
clear_console()

print(colored("[Android_System_killer]", 'red', attrs=['bold']))
print(colored("——————————————————————————————————————————//", 'red'))
time.sleep(1)
printer(colored("[", 'cyan') + colored("!", 'red') + colored("] ", 'cyan') + (color))
print(colored("——————————————————————————————————————————//", 'red'))
print(colored("Ссылка:", 'red', attrs=['bold']))
time.sleep(1)
printer(colored("[", 'cyan') + colored("L", 'red') + colored("] Link: ", 'cyan') + colored(config.link, 'cyan'))

Enter = input(colored("\nНажмите Enter для возвращения в главное меню...", 'cyan'))

if Enter == '':
    open_file(config.menu)