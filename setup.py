import time
import random
import os
import sys
import subprocess
project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'llib'))
sys.path.append(project_path)
import config
from termcolor import colored

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    
clear_console()

def open_file(file_path):
    subprocess.run(['python', file_path])
    
print(colored("Пользовательское соглашение!", 'cyan'))
print(colored("—————————————————————————————————//", 'red'))
print(colored("\nДанная программа создана в развлекательных целях, все действия вы делаете на свой страх и риск, разработчик не несет никакой ответвенности за проблеммы которые могут возникнуть после использования данной программы!", 'cyan'))
print(colored("\nЛюбые сторонние модификации, или перепродажа программы строго запрещенна, в другом случае вам перестанут выдавать и вы будете исключены из всех соц-сетей.", 'cyan'))
print(colored("\nНажимая (Enter) вы принимаете данное соглашение...", 'cyan'))
print(colored("\n—————————————————————————————————//", 'red'))

Enter = input(colored("Нажмите (Enter) для продолжения...", 'yellow'))

if Enter == '':
    open_file(config.loading)