# Импортирование библиотек
import time
import os
import subprocess
import sys
import config
import socket
from termcolor import colored

# Функции меню
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    
clear_console()

def open_file(file_path):
    subprocess.run(['python', file_path])

def open_bash(file):
    subprocess.run(['bash', file])

def check_internet():
    try:
        socket.create_connection(("8.8.8.8", 53), timeout=5)
    except OSError:
        print(colored("Connection_lost!", 'red'))
        print(colored("——————————————————————//", 'red'))
        print(colored("Отсутствует подключение к интернету!", 'red'))
        print(colored("Пожалуйста проверьте ваше подключение к интернету.", 'red'))
        time.sleep(4)
        open_file(config.menu)
        
logo = """██╗  ██╗ █████╗ ██╗  ██╗
╚██╗██╔╝██╔══██╗██║ ██╔╝  ██╗
 ╚███╔╝ ███████║█████╔╝ ██████╗
 ██╔██╗ ██╔══██║██╔═██╗ ╚═██╔═╝
██╔╝ ██╗██║  ██║██║  ██╗  ╚═╝
╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝"""

# Отображение
print(colored(logo, 'red'))
print(colored("————————————————————————————————//", 'red'))
print(colored("Version: build-1002", 'red'))
print(colored("————————————————————————————————//", 'red'))
print(colored("[", 'cyan') + colored("1", 'red') + colored("] ", 'cyan') + colored("Игра", 'cyan'))
print(colored("[", 'cyan') + colored("2", 'red') + colored("] ", 'cyan') + colored("Вирус", 'cyan'))
print(colored("[", 'cyan') + colored("3", 'red') + colored("] ", 'cyan') + colored("Спам на номер", 'cyan'))
print(colored("[", 'cyan') + colored("4", 'red') + colored("] ", 'cyan') + colored("Обновить программу", 'cyan'))

print(colored("————————————————————————————————//", 'red'))
console = input(colored("Введите/> ", 'cyan'))

# Основа
if console == '1':
    open_file(config.play)

elif console == '2':
    open_file(config.virus)
    
elif console == '3':
    clear_console()
    check_internet()
    open_file(config.spam)
    
elif console == '4':
    clear_console()
    check_internet()
    open_bash(config.update)
