import os
import sys
import time
import hashlib
import config
import subprocess
import socket
from rich.console import Console
from rich.progress import Progress
from termcolor import colored

def open_file(file_path):
    subprocess.run(['python', file_path])

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')

clear_console()

def printer(text, delay=0.035):
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(delay)
    print()

def check_internet():
    try:
        socket.create_connection(("8.8.8.8", 53), timeout=5)
    except OSError:
        print(colored("[", 'red') + colored("-", 'red') + colored("]", 'red') + colored(" Статус: нет подключения!", 'red'))
    else:
        print(colored("[", 'green') + colored("+", 'green') + colored("]", 'green') + colored(" Статус: Подключено!", 'green'))

def calculate_sha256(file_path):
    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()

def generate_checksums(directory, checksum_file):
    checksums = {}
    try:
        for root, _, files in os.walk(directory):
            for file in files:
                file_path = os.path.join(root, file)
                # Исключаем сам файл checksum_file из проверки
                if file_path != checksum_file:
                    checksums[file_path] = calculate_sha256(file_path)
        
        with open(checksum_file, "w") as f:
            for file_path, checksum in checksums.items():
                f.write(f"{file_path} {checksum}\n")
    except Exception as e:
        sys.exit(1)

def verify_checksums(checksum_file):
    if not os.path.exists(checksum_file):
        print(f"Checksum file {checksum_file} not found")
        sys.exit(1)

    all_ok = True
    try:
        with open(checksum_file, "r") as f:
            checksums = f.readlines()
        
        for line in checksums:
            parts = line.strip().rsplit(' ', 1)
            if len(parts) != 2:
                all_ok = False
                continue
            
            file_path, expected_checksum = parts
            if not os.path.exists(file_path):
                print(f"Ошибка: файл {file_path} отсутствует")
                all_ok = False
                continue
            
            actual_checksum = calculate_sha256(file_path)
            if actual_checksum != expected_checksum:
                all_ok = False
    except Exception as e:
        sys.exit(1)
    
    if all_ok:
        pass
    else:
        print(colored("[", 'red') + colored("+", 'red') + colored("]", 'red') + colored(" Ошибка: некоторые файлы имеют неполадки или отсутствуют в директории, пожалуйста переустановите программу!", 'red'))
        sys.exit(1)

print(colored("Загрузка...", 'cyan'))
print(colored("————————————————————————————————//", 'red'))

with Progress() as progress:
    
    task1 = progress.add_task("[cyan]Progress:", total=50)
    
    while not progress.finished:
        
        time.sleep(2)
        print(colored("[", 'cyan') + colored("/", 'red') + colored("] ", 'cyan') + colored("Загрузка файлов: (cache)...", 'cyan'))
        time.sleep(3)
        
        progress.update(task1, advance=10)
        time.sleep(0.2)
        
        print(colored("[", 'cyan') + colored("+", 'red') + colored("] ", 'cyan') + colored("Загрузка файла конфигурации: (config)...", 'cyan'))
        time.sleep(3)
        
        progress.update(task1, advance=10)
        time.sleep(0.2)

        print(colored("[", 'cyan') + colored("#", 'red') + colored("] ", 'cyan') + colored("Подключение к серверу: (client_state)...", 'cyan'))
        time.sleep(5)
 
        check_internet()
        time.sleep(2)
        
        progress.update(task1, advance=10)
        time.sleep(0.2)
        
        print(colored("[", 'cyan') + colored("-", 'red') + colored("] ", 'cyan') + colored("Проверка целосности приложения...", 'cyan'))
        time.sleep(4)
        
        # Укажите здесь директорию с файлами, для которых нужно создать контрольные суммы
        directory_to_scan = "/data/data/com.termux/files/home/DarkPluse"
        # Укажите путь к файлу с контрольными суммами
        checksum_file_path = "/data/data/com.termux/files/home/DarkPluse/checksums.txt"

        # Генерация контрольных сумм
        generate_checksums(directory_to_scan, checksum_file_path)

        # Проверка целостности файлов
        verify_checksums(checksum_file_path)
        time.sleep(2)
        
        progress.update(task1, advance=10)
        time.sleep(0.2)
        
        print(colored("[", 'cyan') + colored("×", 'red') + colored("] ", 'cyan') + colored("Запуск...", 'cyan'))
        print(colored("————————————————————————————————//", 'red'))
        
        progress.update(task1, advance=10)
        time.sleep(0.2)
        
        time.sleep(2)
open_file(config.menu)